﻿using api_proxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_proxy
{
    public static class xkcdApiHelper
    {
        public static async Task<BrowseComic> GetToday()
        {
            Comic todaysComic = await GetComic(null);
            BrowseComic browser = new BrowseComic();

            browser.CurrentComic = todaysComic;

            if (todaysComic.ComicNumber != 0)
            {
                var previousComic = await GetComic(todaysComic.ComicNumber - 1);

                browser.PreviousComic = previousComic.ComicNumber == 0
                    ? todaysComic.ComicNumber - 1
                    : previousComic.ComicNumber;
            }


            return browser;

        }

        public static async Task<BrowseComic> GoToComic(int comicNumber)
        {
            int previousNum = comicNumber - 1
                , nextNum = comicNumber + 1;

            Comic
                current = await GetComic(comicNumber)

                , previous =
                    await GetComic(previousNum)
                    ?? await GetComic(--previousNum)

                , next =
                    await GetComic(nextNum)
                    ?? await GetComic(++nextNum);

            BrowseComic browser = new BrowseComic();
            browser.CurrentComic = current;
            browser.PreviousComic = previous == null
                ? 0
                : previous.ComicNumber;
            browser.NextComic = next == null
                ? 0
                : next.ComicNumber;

            return browser;
        }

        private static async Task<Comic> GetComic(int? comicNum)
        {
            var dict = await new RequestBuilder().DoRequest(comicNum);
            if (dict == null) return null;

            Comic comic = new Comic();

            if (dict.ContainsKey("num"))
                comic.ComicNumber = int.Parse(dict["num"]);

            if (dict.ContainsKey("alt"))
                comic.AltText = dict["alt"];

            if (dict.ContainsKey("img"))
                comic.ImageUrl = dict["img"];

            if(dict.ContainsKey("title"))
            {
                comic.Title = dict["title"];
            }

            return comic;
        }
    }
}
