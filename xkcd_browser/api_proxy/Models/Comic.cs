﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_proxy.Models
{
    public class Comic
    {
        public int ComicNumber { get; set; }
        public string ImageUrl { get; set; }
        public string AltText { get; set; }
        public string Title { get; set; }
    }
}
