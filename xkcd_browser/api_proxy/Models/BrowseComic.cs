﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_proxy.Models
{
    public class BrowseComic
    {
        public int PreviousComic { get; set; }
        public Comic CurrentComic { get; set; }
        public int NextComic { get; set; }

        public bool IsTodaysComic
        {
            get
            {
                return CurrentComic?.ComicNumber != 0
                    && NextComic == 0
                    && PreviousComic != 0;
            }
        }

        public bool IsEmptyComic
        {
            get
            {
                return CurrentComic?.ComicNumber == 0;
            }
        }

        public bool IsFirstComic
        {
            get
            {
                return CurrentComic?.ComicNumber != 0
                    && PreviousComic== 0
                    && NextComic != 0;
            }
        }
    }
}
