﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace api_proxy
{
    public class RequestBuilder
    {
        public string Url { get; private set; }
        public int IdRequest { get; private set; }
        public Dictionary<string, string> Result { get; private set; }

        private static HttpClient _client = new HttpClient();



        public async Task<Dictionary<string, string>> DoRequest(int? id)
        {
            Url = ConfigurationManager.AppSettings["baseUri"];
            string comicInfo = ConfigurationManager.AppSettings["infoRq"];

            UriBuilder build = new UriBuilder(Url);
            build.Path = System.IO.Path.Combine(
                id.HasValue
                    ? id.Value.ToString()
                    : string.Empty
                , comicInfo);

            return await Do(build.ToString());
        }


        private async Task<Dictionary<string, string>> Do(string url)
        {
            var httResponse = await _client.GetAsync(url);

            if (httResponse.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<Dictionary<string, string>>(await httResponse.Content.ReadAsStringAsync());

            else return null;
        }
    }
}
