﻿using api_proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace xkcd_browser.Controllers
{
    public class BrowserController : Controller
    {
    
        [HandleError(View = "Yikes")]
        public async Task<ActionResult> comic(int? id)
        {
            
            RequestBuilder builder = new RequestBuilder();
            var comicBrowser = id == null || !id.HasValue ?
                await xkcdApiHelper.GetToday()
                : await xkcdApiHelper.GoToComic(id.Value);

            if (comicBrowser.CurrentComic == null)
                return View("v404");

            return View("browser", comicBrowser);
        }
    }
}