﻿using System.Web.Mvc;
using System.Web.Routing;

namespace xkcd_browser
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "jic",
                url: "browser/browser",
                defaults: new { controller = "browser", action = "comic", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "comic",
                url: "comic/{id}",
                defaults: new { controller = "browser", action = "comic", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "browser", action = "comic", id = UrlParameter.Optional }
            );
        }
    }
}
